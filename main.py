from destiny import Destiny

def calculate_matching_destiny(method, path, destinies):
	result = None
	matched = 0
	for d in destinies:
		if d.Matches(method, path):
			result = d
			matched += 1
	assert(matched <= 1)
	return result

def parse_line(line):
	fields = line.split(" ")
	assert(len(fields) == 12)
	keys = []
	values = []
	for field in fields[2:]: #skip date, time and server name
		data = field.split("=")
		assert(len(data) == 2)
		keys.append(data[0])
		values.append(data[1])
	return (keys, values)

def create_destinies(destinies_addresses):
	destinies = []
	for da in destinies_addresses:
		(method, post) = da
		destiny = Destiny(method, post)
		destinies.append(destiny)
	return destinies

destinies_addresses = [("GET", "/api/users/{user_id}/count_pending_messages"),
			("GET", "/api/users/{user_id}/get_messages"),
			("GET", "/api/users/{user_id}/get_friends_progress"),
			("GET", "/api/users/{user_id}/get_friends_score"),
			("POST", "/api/users/{user_id}"),
			("GET", "/api/users/{user_id}")]

destinies = create_destinies(destinies_addresses) 

file = open("sample.log", "rt")
#file = open("little_sample.log", "rt")     
line = file.readline()

while not line == "":
	#print line
	(keys, values) = parse_line(line)
	assert(keys[1] == "method" and keys[2] == "path")
	method = values[1]
	path = values[2]
	destiny = calculate_matching_destiny(method, path, destinies)	
	if destiny != None:			
		destiny.IncrementCalls()
		
		assert(keys[6] == "connect" and keys[7] == "service")
		response_time = int(values[6][0:len(values[6]) - 2]) + int(values[7][0:len(values[7]) - 2]) #skip 'ms'
		destiny.AddResponseTime(response_time)
		
		assert(keys[5] == "dyno")
		dyno = values[5]
		destiny.IncrementDyno(dyno)
		
	line = file.readline()

for destiny in destinies:
	destiny.ProcessData()