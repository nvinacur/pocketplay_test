class Destiny:
	def __init__(self, method, path):
		self.method = method
		self.path = path
		self.calls = 0
		self.dynos = {}
		self.response_times = []
	
	def IncrementCalls(self):
		self.calls += 1
		
	def Matches(self, method, concrete_path):
		if method != self.method:
			return False
		dirs_a = self.path.split("/")
		dirs_c = concrete_path.split("/")
		if len(dirs_a) != len(dirs_c):
			return False
		for i in xrange(len(dirs_a)):
			directory = dirs_a[i]
			if directory != "{user_id}" and directory != dirs_c[i]:
				return False
		return True

	def IncrementDyno(self, dyno):
		value = self.dynos.get(dyno)
		if value == None:
			value = 0
		self.dynos[dyno] = value + 1
	
	def AddResponseTime(self, time):
		self.response_times.append(time)
		
	def __CalculateResponseTimeStatistics(self):
		mean = -1
		mode = -1
		median = -1
		self.response_times.sort()
		n = len(self.response_times)
		if n > 0:
			times_shown = {}
			mean = 0
			for rs in self.response_times:
				mean += rs
				ts = times_shown.get(rs)
				if ts == None:
					ts = 0
				times_shown[rs] = ts + 1
			mean /= n
			if n % 2 == 1:
				median = self.response_times[n / 2]
			else:
				median = (self.response_times[n / 2 - 1] + self.response_times[n / 2]) / 2.0
			max_value = -1			
			for k in times_shown.keys():
				v = times_shown[k]
				if v > max_value:
					max_value = v
					max_key = k
			mode = max_key
		return (mean, median, mode)
	
	def __CalculateTopDyno(self):
		top_dyno = ""
		top_dyno_score = -1
		for dyno in self.dynos.keys():
			value = self.dynos[dyno]
			if value > top_dyno_score:
				top_dyno = dyno 
		return top_dyno
	
	def __Format(self, f):
		if f == -1:
			return "NaN"
		else:
			return str(f)
	
	def ProcessData(self):
		(mean, median, mode) = self.__CalculateResponseTimeStatistics()
		top_dyno = self.__CalculateTopDyno()
		title = "method: " + self.method + " path: " + self.path
		print title
		print "-" * len(title)
		print "times called: %d" % self.calls
		print "response time mean: %s" % self.__Format(mean)
		print "response time median: %s" % self.__Format(median)
		print "response time mode: %s" % self.__Format(mode)
		print "dyno that responded the most: %s" % top_dyno
		print
		